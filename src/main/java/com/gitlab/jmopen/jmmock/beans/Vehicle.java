package com.gitlab.jmopen.jmmock.beans;

/**
 * Created by liujiming on 2016/11/29.
 */
public interface Vehicle {

    String getName();

    int getPrice();

    boolean hasWheel();

    boolean isCanFly();

    boolean isCanTravelOnRoad();

    boolean isCanTravelOnWater();

    boolean isCanTravelUnderWater();

}
