package com.gitlab.jmopen.jmmock.beans;

import com.gitlab.jmopen.jmmock.utils.EasyRandomUtils;
import lombok.Data;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by liujiming on 2016/11/29.
 */
@Data
public class Bus extends BaseCar implements Car{

//    @Min(1)
    @Max(1000)
//    @Positive
    private int passengerCount;

    @Override
    public int getWheelCount() {
        return 6;
    }


    public static Bus random() {
        Bus result = EasyRandomUtils.getDefaultEasyRandom().nextObject(Bus.class);
        return result;
    }

    public static List<Bus> random(int count) {
        List<Bus> result = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            result.add(random());
        }
        return result;
    }

}
