package com.gitlab.jmopen.jmmock.beans;

import lombok.Data;
import org.apache.commons.lang3.RandomUtils;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by liujiming on 2016/11/29.
 */
@Data
public class Wheel {

    @Max(value = 2000L)
    @Min(value = 90L)
    private int diameterInMillimeter;




    public static Wheel mock(int i) {
        Wheel wheel = new Wheel();
        wheel.setDiameterInMillimeter(i);
        return wheel;
    }

    public static Wheel random() {
        Wheel wheel = new Wheel();
        wheel.setDiameterInMillimeter(RandomUtils.nextInt(20, 100));
        return wheel;
    }

    public static List<Wheel> random(int count) {
        return Stream.generate(Wheel::random).limit(count).collect(Collectors.toList());
    }

}
