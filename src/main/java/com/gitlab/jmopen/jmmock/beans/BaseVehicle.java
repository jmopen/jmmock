package com.gitlab.jmopen.jmmock.beans;

import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

/**
 * Created by liujiming on 2016/11/29.
 */
@Data
public abstract class BaseVehicle implements Vehicle{
    private String name;
    private int price;


    public static void mock(BaseVehicle vehicle, int i) {
        vehicle.setName("name_" + i);
        vehicle.setPrice(i);
    }


    public static void random(BaseVehicle vehicle) {
        vehicle.setName("name_" + RandomStringUtils.randomAlphabetic(6));
        vehicle.setPrice(RandomUtils.nextInt(30_000, 10_000_000));
    }


}
