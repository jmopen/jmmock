package com.gitlab.jmopen.jmmock.beans;



import java.util.List;

/**
 * Created by liujiming on 2016/11/29.
 */
public interface Car extends Vehicle {

    int getWheelCount();

    List<Wheel> getWheelList();

    CarEngine getEngine();

    @Override
    default boolean hasWheel() {return true;}

    @Override
    default boolean isCanFly() {return false;}

    @Override
    default boolean isCanTravelOnRoad() {return true;}

    @Override
    default boolean isCanTravelOnWater() {return false;}

    @Override
    default boolean isCanTravelUnderWater() {return false;}
}
