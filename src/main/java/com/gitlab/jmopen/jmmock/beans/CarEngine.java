package com.gitlab.jmopen.jmmock.beans;

import lombok.Data;

/**
 * Created by liujiming on 2016/11/29.
 */
@Data
public class CarEngine implements Engine {
    private String manufactory;
    private float power;

    

}
