package com.gitlab.jmopen.jmmock.beans;

import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by liujiming on 2016/11/29.
 */
@Data
public abstract class BaseCar extends BaseVehicle implements Car{
    private CarEngine engine;
    @Size(min = 4, max = 20)
    private List<Wheel> wheelList;


    public static void mock(BaseCar vehicle, int i) {
        BaseVehicle.mock(vehicle, i);
        vehicle.setName("name_" + i);
        vehicle.setPrice(i);
    }


    public static void random(BaseCar vehicle) {
        BaseVehicle.random(vehicle);
        vehicle.setName("name_" + RandomStringUtils.randomAlphabetic(6));
        vehicle.setPrice(RandomUtils.nextInt(30_000, 10_000_000));
    }
}
