package com.gitlab.jmopen.jmmock.beans;

/**
 * Created by liujiming on 2016/11/29.
 */
public interface Ship extends Vehicle {
    @Override
    default boolean hasWheel() {return false;}

    @Override
    default boolean isCanFly() {return false;}

    @Override
    default boolean isCanTravelOnRoad() {return false;}

    @Override
    default boolean isCanTravelOnWater() {return true;}

    @Override
    default boolean isCanTravelUnderWater() {return false;}
}
