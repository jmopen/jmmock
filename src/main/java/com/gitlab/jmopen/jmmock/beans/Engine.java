package com.gitlab.jmopen.jmmock.beans;

/**
 * Created by liujiming on 2016/11/29.
 */
public interface Engine {
    String getManufactory();

    float getPower();
}
