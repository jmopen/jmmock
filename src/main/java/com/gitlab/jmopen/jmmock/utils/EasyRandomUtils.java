package com.gitlab.jmopen.jmmock.utils;

import com.github.javafaker.Faker;
import com.google.common.base.Charsets;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;

import java.util.Locale;

/**
 * Created by Jimmy.Liu at 2021/11/15 19:53
 */
public class EasyRandomUtils {
    private static EasyRandomParameters parameters;
    private static EasyRandom random;
    private static Faker faker;
    private static Faker fakerSimplifiedChinese;
    static {
        parameters = new EasyRandomParameters()
                .seed(System.currentTimeMillis())
                .objectPoolSize(100)
                .randomizationDepth(3)
                .charset(Charsets.UTF_8)
                .stringLengthRange(5, 50)
                .collectionSizeRange(1, 10)
                .scanClasspathForConcreteTypes(true)
                .overrideDefaultInitialization(false)
                .ignoreRandomizationErrors(true);
        random = new EasyRandom(parameters);
        faker = Faker.instance(Locale.US);
        fakerSimplifiedChinese = Faker.instance(Locale.SIMPLIFIED_CHINESE);
    }

    public static EasyRandomParameters getDefaultEasyRandomParameters() {
        return EasyRandomUtils.parameters;
    }

    public static EasyRandom getDefaultEasyRandom() {
        return EasyRandomUtils.random;
    }

    public static Faker getDefaultFaker() {
        return EasyRandomUtils.faker;
    }
}
