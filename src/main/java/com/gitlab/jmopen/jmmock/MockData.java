package com.gitlab.jmopen.jmmock;

import com.google.common.collect.ImmutableList;

import java.util.*;

/**
 * Created by liujiming on 2016/11/29.
 */
public class MockData {

    public static Random r = new Random();


    private static String[] sequenceStrings = {"first", "second", "third", "forth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"};
    private static String[] orderedStr= new String[]{"aa", "bb", "cc", "dd", "ee", "ff", "gg", "hh", "ii", "jj"};

    private static List<String> sequenceStringList = ImmutableList.copyOf(sequenceStrings);

    private static List<String> carBrandList = ImmutableList.<String>builder()
            .add("Benz", "BMW", "Volvo", "Subaru", "Audi", "Skoda", "Buick", "Land rover", "Infiniti")
            .build();

    public static String[] getSequenceStrings() {
        return sequenceStrings.clone();
    }

    public static List<String> getSequenceStringList() {
        return sequenceStringList;
    }

    public static List<String> getCarBrandList() {
        return carBrandList;
    }


    public static String randomSequence() {
        return sequenceStrings[r.nextInt(sequenceStrings.length)];
    }



    public static String randomCarBrand() {
        return carBrandList.get(r.nextInt(carBrandList.size()));
    }


    static Map<String,Integer> ordinalNumberMap = new HashMap<String,Integer>();

    static{
        ordinalNumberMap = new HashMap<String,Integer>();
        ordinalNumberMap.put("first", 1);
        ordinalNumberMap.put("second", 2);
        ordinalNumberMap.put("third", 3);
        ordinalNumberMap.put("fourth", 4);
        ordinalNumberMap.put("fifth", 5);
        ordinalNumberMap.put("sixth", 6);
        ordinalNumberMap.put("seventh", 7);
        ordinalNumberMap.put("eighth", 8);
        ordinalNumberMap.put("ninth", 9);
        ordinalNumberMap.put("tenth", 10);
        ordinalNumberMap = Collections.unmodifiableMap(ordinalNumberMap);
    }

    public static String[] getOrdinalNumber10(){
        return sequenceStrings.clone();
    }

    public static String[] getOrderedStr10(){
        return orderedStr.clone();
    }

    @SuppressWarnings("unchecked")
    public static Map<String,Integer> getOrdinalNumberMap10(){
        return ordinalNumberMap;
    }


}
