package com.gitlab.jmopen.jmmock.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import com.fasterxml.jackson.module.blackbird.BlackbirdModule;

/**
 * Created by Jimmy.Liu at 2021/11/16 10:32
 */
public class JsonUtils {

    private static ObjectMapper objectMapper;

    static {
        objectMapper = JsonMapper.builder()
//                .addModule(new AfterburnerModule())
//                .addModule(new BlackbirdModule())
                .build();

    }


    public static String toJson(Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
        }
        return "";
    }


}
