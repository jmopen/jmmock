package com.gitlab.jmopen.jmmock.beans;


import com.devskiller.jfairy.Fairy;
import com.github.javafaker.Company;
import com.github.javafaker.Faker;
import com.gitlab.jmopen.jmmock.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import java.util.List;
import java.util.Locale;


/**
 * Created by liujiming on 2016/11/29.
 */
@Slf4j
public class WheelTest {

    
    @Test
    public void test_random_n () {
        List<Wheel> wheels = Wheel.random(6);
        List<Bus> buses = Bus.random(1);
        System.out.println(buses);
        System.out.println("------------------");
        System.out.println(JsonUtils.toJson(buses));
//        assertEquals(wheels.size(), 6);
//        Fairy fairy = Fairy.create();
//        Company company = fairy.company();
//        System.out.println(JsonUtils.toJson(company));
        Faker faker = Faker.instance();
        Company company = faker.company();
        System.out.println("------------------");
        System.out.println(JsonUtils.toJson(company.toString()));
        RuntimeException exception = new RuntimeException("Something is wrong");
        log.error("Big error {}", exception);

        System.out.println("------------------");
        log.error("Big error {}", "发生了", exception);

    }

}



