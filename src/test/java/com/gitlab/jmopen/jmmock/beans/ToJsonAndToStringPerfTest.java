package com.gitlab.jmopen.jmmock.beans;

import com.devskiller.jfairy.Fairy;
import com.gitlab.jmopen.jmmock.utils.JsonUtils;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.Runner;


import java.util.concurrent.TimeUnit;

/**
 * Created by Jimmy.Liu at 2021/11/16 15:10
 */
@BenchmarkMode(Mode.AverageTime) // 吞吐量
@OutputTimeUnit(TimeUnit.MILLISECONDS) // 结果所使用的时间单位
@State(Scope.Thread) // 每个测试线程分配一个实例
@Fork(2) // Fork进行的数目
@Warmup(iterations = 2) // 先预热4轮
@Measurement(iterations = 4) // 进行10轮测试
public class ToJsonAndToStringPerfTest {



//    @Param({"10", "40", "70", "100"}) // 定义四个参数，之后会分别对这四个参数进行测试
//    private int n;

    private static Bus[] buses;

    @Setup(Level.Trial) // 初始化方法，在全部Benchmark运行之前进行
    public void init() {
        int count = 100;
        buses = new Bus[count];
        for (int i = 0; i < count; i++) {
            buses[i] = Bus.random();
        }
    }

    @Benchmark
    public void test_toJson() {
        for (int i = 0; i < buses.length; i++) {
            JsonUtils.toJson(buses[i]);
        }
    }

    @Benchmark
    public void test_toString() {
        for (int i = 0; i < buses.length; i++) {
            buses[i].toString();
        }
    }

    @TearDown(Level.Trial) // 结束方法，在全部Benchmark运行之后进行
    public void arrayRemove() {

    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder().include(ToJsonAndToStringPerfTest.class.getSimpleName()).build();
        new Runner(options).run();
    }

}
